import UIKit

class CustomTableViewCell: UITableViewCell {

    // MARK: - IBOutlets

    @IBOutlet weak var customLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
