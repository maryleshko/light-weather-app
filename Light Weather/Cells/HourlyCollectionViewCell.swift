import UIKit

class HourlyCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets

    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    
    // MARK: - Flow functions

    func setup(with image: String?,  text: String?, temp: Double?) {
        iconImageView.image = UIImage(named: image ?? "")
        iconImageView.contentMode = .scaleAspectFill
        hourLabel.text = text
        let newTemp = Int(temp?.rounded() ?? 0.0)
        tempLabel.text = String(newTemp) + " °"
    }
}
