import Foundation

class FirstControllerViewModel {
    
    var label = Bindable<String>("")
    var imageName = Bindable<String>("")
    var fontName = Bindable<String>("")
    var loadingText = Bindable<String>("")
    
    func update() {
        self.label.value = "Light Weather"
        self.imageName.value = "menu"
        self.fontName.value = "Gladish"
        self.loadingText.value = "Loading..."
    }
    
}
