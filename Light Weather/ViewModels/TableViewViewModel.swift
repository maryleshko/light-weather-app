import Foundation

class TableViewViewModel {
    
    var cityArray = WeatherArrayManager.shared.cityArray
    var textBackToMenu = Bindable<String>("")
    var searchBarPlaceholder = Bindable<String>("")
    
    func update() {
        self.textBackToMenu.value = "BACK".localized()
        self.searchBarPlaceholder.value = "Search city".localized()
    }
    
}
