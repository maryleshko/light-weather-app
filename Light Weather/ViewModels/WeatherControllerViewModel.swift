import Foundation

class WeatherControllerViewModel {
    
    var index = Bindable<Int>(0)
    var fullWeather = Bindable<[ControllersModel]>([ControllersModel]())
    var weather = Bindable<ControllersModel>(ControllersModel(currentWeather: WeatherInfo(date: "", city: "", main: "", description: "", temp: 0.0, maxTemp: 0.0, nightTemp: 0.0, visibility: 0, pressure: 0, humidity: 0, sunrise: "", sunset: "", clouds: 0, rain: 0.0, icon: "", wind_speed: 0.0, wind_deg: 0, feels_like: 0.0), hourlyWeather:[], dailyWeather: []))
    var currentWeather = Bindable<WeatherInfo>(WeatherInfo(date: "", city: "", main: "", description: "", temp: 0.0, maxTemp: 0.0, nightTemp: 0.0, visibility: 0, pressure: 0, humidity: 0, sunrise: "", sunset: "", clouds: 0, rain: 0.0, icon: "", wind_speed: 0.0, wind_deg: 0, feels_like: 0.0))
    var hourlyWeather = Bindable<[HourlyWeather]>([HourlyWeather]())
    var dailyWeather = Bindable<[DailyWeather]>([DailyWeather]())
    var city = Bindable<String>("")
    var temp = Bindable<Double>(0.0)
    var minTemp = Bindable<Double>(0.0)
    var maxTemp = Bindable<Double>(0.0)
    var description = Bindable<String>("")
    var date = Bindable<String>("")
    var feelsDescription = Bindable<String>("")
    var feelsValue = Bindable<Double>(0.0)
    var tempString = Bindable<String>("")
    var minTempString = Bindable<String>("")
    var maxTempString = Bindable<String>("")
    var feelsLikeString = Bindable<String>("")
    var cityLocalized = Bindable<String>("")
    var sunriseString = Bindable<String>("")
    var sunsetString = Bindable<String>("")
    var humidityString = Bindable<String>("")
    var pressureString = Bindable<String>("")
    var pressureDescr = Bindable<String>("")
    var percent = Bindable<String>("")
    var visibilityString = Bindable<String>("")
    var visibilityDescr = Bindable<String>("")
    var windString = Bindable<String>("")
    var windSpeedString = Bindable<String>("")
    var north = Bindable<String>("")
    var east = Bindable<String>("")
    var south = Bindable<String>("")
    var west = Bindable<String>("")
    var rainString = Bindable<String>("")
    var cloudsString = Bindable<String>("")
    
    func setupHourlyWeather() {
        
    }
    func setAray(_ array: [ControllersModel]) {
        self.fullWeather.value = array
    }
    func setIndex(_ index: Int) {
        self.index.value = index
    }
    func update() {
        self.fullWeather.value = WeatherArrayManager.shared.locationArray + WeatherArrayManager.shared.fullWeatherArray
        self.weather.value = fullWeather.value[self.index.value]
        self.currentWeather.value = self.weather.value.currentWeather ?? WeatherInfo(date: "", city: "", main: "", description: "", temp: 0.0, maxTemp: 0.0, nightTemp: 0.0, visibility: 0, pressure: 0, humidity: 0, sunrise: "", sunset: "", clouds: 0, rain: 0.0, icon: "", wind_speed: 0.0, wind_deg: 0, feels_like: 0.0)
        self.hourlyWeather.value = self.weather.value.hourlyWeather ?? []
        self.dailyWeather.value = self.weather.value.dailyWeather ?? []
    }
    func setValues() {
        self.city.value = self.weather.value.currentWeather?.city ?? ""
        self.temp.value = self.weather.value.currentWeather?.temp ?? 0.0
        self.tempString.value = "\(String(Int(self.temp.value.rounded() ))) °"
        self.minTemp.value = self.weather.value.currentWeather?.nightTemp ?? 0.0
        self.minTempString.value = "\(String(Int(self.minTemp.value.rounded() ))) °"
        self.maxTemp.value = self.weather.value.currentWeather?.maxTemp ?? 0.0
        self.maxTempString.value = "\(String(Int(self.maxTemp.value.rounded() ))) °"
        self.description.value = self.weather.value.currentWeather?.description ?? ""
        self.date.value = self.weather.value.currentWeather?.date ?? ""
        self.feelsDescription.value = "Feels like:".localized()
        self.feelsValue.value = self.weather.value.currentWeather?.feels_like ?? 0.0
        self.feelsLikeString.value = "\(String(Int(self.feelsValue.value.rounded() ))) °"
        self.cityLocalized.value = "CURRENT LOCATION".localized()
        
        self.sunriseString.value = "Sunrise".localized()
        self.sunsetString.value = "Sunset".localized()
        self.humidityString.value = "Humidity".localized()
        self.percent.value = " %"
        self.pressureString.value = "Pressure".localized()
        self.pressureDescr.value = "hPa".localized()
        self.visibilityString.value = "Visibility".localized()
        self.visibilityDescr.value = "km".localized()
        self.windString.value = "Wind".localized()
        self.windSpeedString.value = "km/h".localized()
        self.north.value = "N".localized()
        self.east.value = "E".localized()
        self.south.value = "S".localized()
        self.west.value = "W".localized()
        self.rainString.value = "Rain".localized()
        self.cloudsString.value = "Clouds".localized()
        
    }
    
    func updateControllers(_ index: Int) {
        var changedArray = WeatherArrayManager.shared.fullWeatherArray
        changedArray.remove(at: index - 1)
        var coordArray = SaveWeatherManager.shared.getCoordinatesArray()
        coordArray?.remove(at: index - 1)
        WeatherArrayManager.shared.fullWeatherArray = changedArray
        SaveWeatherManager.shared.setCoordinatesArray(coordArray ?? [])
    }
    
    
}
