import UIKit

class ResultView: UIView {
    
    // MARK: - IBOutlets

    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunriseResultLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var sunsetResultLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var humidityResultLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var pressureResultLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var visibilityResultLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var windResultLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var rainResultLabel: UILabel!
    @IBOutlet weak var cloudsLabel: UILabel!
    @IBOutlet weak var cloudsResultLabel: UILabel!
    
    static func instanceFromNib() -> ResultView {
        return UINib(nibName: "ResultView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? ResultView ?? ResultView()
        
    }

}
