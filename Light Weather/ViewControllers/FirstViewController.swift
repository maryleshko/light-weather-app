import UIKit
import CoreLocation
import FirebaseCrashlytics

class FirstViewController: UIViewController, CLLocationManagerDelegate {
    
    // MARK: - IBOutlets

    @IBOutlet weak var logoLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var loadingLabel: UILabel!
    
    // MARK: - Public Properties

    var array = [City]()
    var sortedArray = [City]()
    var manager = CLLocationManager()
    var name = String()
    var coordinates = CLLocationCoordinate2D()
    var currentLocation = CityCoordinates()
    var coordinatesArray = [CityCoordinates]()
    var viewModel: FirstControllerViewModel?
    var counter = 0
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = FirstControllerViewModel()
        viewModel?.update()
        self.bindOutlets()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        
        DispatchQueue.global().async {
            JSONManager.shared.openJSON { (_) in
                self.coordinatesArray = SaveWeatherManager.shared.getCoordinatesArray() ?? []
        
                if self.coordinatesArray.count == 0 {
                    JSONManager.shared.locationOpenWeatherJSON(lat: self.coordinates.latitude, lon: self.coordinates.longitude, name: self.name) { (_) in
                        DispatchQueue.main.async {
                            
                            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewPageViewController") as? NewPageViewController else {return}
                            controller.modalPresentationStyle = .fullScreen
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                } else {
                    JSONManager.shared.locationOpenWeatherJSON(lat: self.coordinates.latitude, lon: self.coordinates.longitude, name: self.name) { (_) in
                    print("everything is ok")
                    }
                    for element in self.coordinatesArray {
                        JSONManager.shared.secondOpenWeatherJSON(lat: element.coordinates?.lat ?? 0.0, lon: element.coordinates?.lon ?? 0.0, name: element.name ?? "") { (_) in
                            print("everything is ok")
                            self.counter += 1
                            if self.counter == self.coordinatesArray.count {
                            DispatchQueue.main.async {
                                guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewPageViewController") as? NewPageViewController else {return}
                                controller.modalPresentationStyle = .fullScreen
                                self.navigationController?.pushViewController(controller, animated: true)
                            }
                            }
                        }
                    }
                }
            }
        }
        
        addParallaxToView(view: mainImageView, magnitude: 30)
        mainImageView.contentMode = .scaleAspectFill
        
    }
    // MARK: - Private functions

    private func bindOutlets() {
        self.viewModel?.imageName.bind({ (value) in
            self.mainImageView.image = UIImage(named: value)
        })
        self.viewModel?.label.bind({ (value) in
            self.logoLabel.text = value
        })
        self.viewModel?.fontName.bind({ (value) in
            self.logoLabel.font = UIFont(name: value, size: 50)
            self.loadingLabel.font = UIFont(name: value, size: 30)
        })
        self.viewModel?.loadingText.bind({ (value) in
            self.loadingLabel.text = value
        })
    }
    
    // MARK: - Flow functions

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.coordinates = location.coordinate
        var coord = Coordinates()
        coord.lat = coordinates.latitude
        coord.lon = coordinates.longitude
        currentLocation.coordinates = coord
        self.manager.stopUpdatingLocation()
        for element in self.array {
            if element.coord?.lat == coordinates.latitude, element.coord?.lon == coordinates.longitude {
                self.name = element.name
            } else {
                self.name = ""
            }
        }
        currentLocation.name = name
    }
    func addParallaxToView(view: UIView, magnitude: Float) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -magnitude
        horizontal.maximumRelativeValue = magnitude
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -magnitude
        vertical.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        view.addMotionEffect(group)
    }
}
