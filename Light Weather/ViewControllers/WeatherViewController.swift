import UIKit

// MARK: - Protocols

protocol WeatherViewControllerDelegate: AnyObject {
    func update(_ index: Int)
    func deleteCity(_ index: Int)
}

class WeatherViewController: UIViewController {
    
    // MARK: - IBOutlets

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var describtionLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var dailyTableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var feelsLikeTempLabel: UILabel!
    @IBOutlet weak var feelsLikeTextLabel: UILabel!
    @IBOutlet weak var buttonAddedCity: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var buttonDeletedCity: UIButton!
    @IBOutlet weak var descrContainerView: UIView!
    
    // MARK: - Public Properties

    var hourArray = [HourlyWeather]()
    var dayArray = [DailyWeather]()
    var currentWeather = WeatherInfo(date: "", city: "", main: "", description: "", temp: 0.0, maxTemp: 0.0, nightTemp: 0.0, visibility: 0, pressure: 0, humidity: 0, sunrise: "", sunset: "", clouds: 0, rain: 0.0, icon: "", wind_speed: 0.0, wind_deg: 0, feels_like: 0.0)
    var index: Int?
    var array = [ControllersModel]()
    var viewModel: WeatherControllerViewModel?
    weak var delegate: WeatherViewControllerDelegate?
    override var prefersStatusBarHidden: Bool {
        return true
    }
    let weatherView = ResultView.instanceFromNib()
    let descriptionView = DescriptionView.instanceFromNib()

    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()

        setSettings()
        
        self.createDescriptionView()
        self.createWeatherView()
        
        hourArray.removeFirst()
        

       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK: - IBActions

    @IBAction func buttonDeletedCity(_ sender: UIButton) {
        self.delegate?.deleteCity(self.index ?? 0)

    }
    @IBAction func buttonAddedCity(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
            return
        }
        controller.modalPresentationStyle = .fullScreen
        controller.delegate = self
        controller.newArray = WeatherArrayManager.shared.cityArray
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Private functions

    private func updateValues() {
        self.viewModel?.index.bind({ (value) in
            self.index = value
        })
        self.viewModel?.currentWeather.bind({ (value) in
            self.currentWeather = value
        })
        self.viewModel?.hourlyWeather.bind({ (value) in
            self.hourArray = value
        })
        self.viewModel?.dailyWeather.bind({ (value) in
            self.dayArray = value
        })
        
        
        if self.viewModel?.city.value == "" {
            self.viewModel?.cityLocalized.bind({ (value) in
                self.cityLabel.text = value
            })
        } else {
            self.viewModel?.city.bind({ (value) in
                self.cityLabel.text = value
            })
        }
        self.viewModel?.tempString.bind({ (value) in
            self.currentTempLabel.text = value
        })
        self.viewModel?.minTempString.bind({ (value) in
            self.minTempLabel.text = value
        })
        self.viewModel?.maxTempString.bind({ (value) in
            self.maxTempLabel.text = value
        })
        self.viewModel?.description.bind({ (value) in
            self.describtionLabel.text = value
        })
        self.viewModel?.date.bind({ (value) in
            self.dateLabel.text = value
        })
        self.viewModel?.feelsDescription.bind({ (value) in
            self.feelsLikeTextLabel.text = value
        })
        self.viewModel?.feelsLikeString.bind({ (value) in
            self.feelsLikeTempLabel.text = value
        })
    }
    
    private func updateAlert() {
        self.viewModel?.sunriseString.bind({ (value) in
            self.weatherView.sunriseLabel.text = value
        })
        self.viewModel?.currentWeather.bind({ (value) in
            self.weatherView.sunriseResultLabel.text = value.sunrise
            self.weatherView.sunsetResultLabel.text = value.sunset
            self.weatherView.humidityResultLabel.text = String(value.humidity ?? 0) + " %"
            self.weatherView.pressureResultLabel.text = "\(String(value.pressure ?? 0)) " + "hPa".localized()
            let visibility = value.visibility ?? 0
            let visibilityInKm = visibility / 1000
            self.weatherView.visibilityResultLabel.text = "\(String(visibilityInKm)) " + "km".localized()
            let windDeg = value.wind_deg ?? 0
            let wind_speed = value.wind_speed ?? 0.0
            if windDeg > 0, windDeg < 90 {
                self.weatherView.windResultLabel.text = "\(String(describing: wind_speed)) " + "km/h".localized() + " " + "N".localized()
            } else if windDeg >= 90, windDeg < 180 {
                self.weatherView.windResultLabel.text = "\(String(describing: wind_speed)) " + "km/h".localized() + " " + "E".localized()
            } else if windDeg >= 180, windDeg < 270 {
                self.weatherView.windResultLabel.text = "\(String(describing: wind_speed)) " + "km/h".localized() + " " + "S".localized()
            } else {
                self.weatherView.windResultLabel.text = "\(String(describing: wind_speed)) " + "km/h".localized() + " " + "W".localized()
            }
            self.weatherView.rainResultLabel.text = String(value.rain ?? 0.0)
            self.weatherView.cloudsResultLabel.text = String(value.clouds ?? 0) + " %"


        })
        self.viewModel?.sunsetString.bind({ (value) in
            self.weatherView.sunsetLabel.text = value
        })
        self.viewModel?.humidityString.bind({ (value) in
            self.weatherView.humidityLabel.text = value
        })
        self.viewModel?.pressureString.bind({ (value) in
            self.weatherView.pressureLabel.text = value
        })
        self.viewModel?.visibilityString.bind({ (value) in
            self.weatherView.visibilityLabel.text = value
        })
        self.viewModel?.windString.bind({ (value) in
            self.weatherView.windLabel.text = value
        })
        self.viewModel?.rainString.bind({ (value) in
            self.weatherView.rainLabel.text = value
        })
        self.viewModel?.cloudsString.bind({ (value) in
            self.weatherView.cloudsLabel.text = value
        })
    }
    
    // MARK: - Flow functions

    func setSettings() {
       if currentWeather.city == "" {
            cityLabel.text = "You're here :)".localized()
        } else {
            cityLabel.text = currentWeather.city
        }
        currentTempLabel.text = "\(String(Int(currentWeather.temp?.rounded() ?? 0.0))) °"
        maxTempLabel.text = "\(String(Int(currentWeather.maxTemp?.rounded() ?? 0.0))) °"
        minTempLabel.text = "\(String(Int(currentWeather.nightTemp?.rounded() ?? 0.0))) °"
        describtionLabel.text = currentWeather.description
        dateLabel.text = currentWeather.date
        feelsLikeTempLabel.text = "Feels like:".localized() + " \(String(Int(currentWeather.feels_like?.rounded() ?? 0.0))) °"
//        feelsLikeTextLabel.text = ""


        collectionView.backgroundColor = .clear
        dailyTableView.backgroundColor = .clear
        weatherImageView.contentMode = .scaleAspectFill
        switch currentWeather.icon {
        case "01d":
            weatherImageView.image = UIImage(named: "01dpic")
            makingWhiteText()
        case "01n":
            weatherImageView.image = UIImage(named: "01npic")
            makingWhiteText()
        case "02d":
            weatherImageView.image = UIImage(named: "02dpic")
            makingWhiteText()
        case "02n":
            weatherImageView.image = UIImage(named: "02npic")
            makingWhiteText()
        case "03d":
            weatherImageView.image = UIImage(named: "03dpic")
            makingBlackText()
        case "03n":
            weatherImageView.image = UIImage(named: "03npic")
            makingWhiteText()
        case "04d":
            weatherImageView.image = UIImage(named: "03dpic")
            makingBlackText()
        case "04n":
            weatherImageView.image = UIImage(named: "03npic")
            makingWhiteText()
        case "09d":
            weatherImageView.image = UIImage(named: "09dpic")
            makingWhiteText()
        case "09n":
            weatherImageView.image = UIImage(named: "09npic")
            makingWhiteText()
        case "10d":
            weatherImageView.image = UIImage(named: "09dpic")
            makingWhiteText()
        case "10n":
            weatherImageView.image = UIImage(named: "09npic")
            makingWhiteText()
        case "11d":
            weatherImageView.image = UIImage(named: "11dpic")
            makingBlackText()
        case "11n":
            weatherImageView.image = UIImage(named: "11npic")
            makingWhiteText()
        case "13d":
            weatherImageView.image = UIImage(named: "13dpic")
            makingBlackText()
        case "13n":
            weatherImageView.image = UIImage(named: "13npic")
            makingWhiteText()
        case "50d":
            weatherImageView.image = UIImage(named: "50dpic")
            makingBlackText()
        case "50n":
            weatherImageView.image = UIImage(named: "50npic")
            makingWhiteText()
        default:
            weatherImageView.image = UIImage(named: "menu")
            makingWhiteText()
        }
    }
    func createWeatherView() {
        weatherView.frame = self.containerView.bounds
    weatherView.sunriseLabel.text = "Sunrise".localized()
    weatherView.sunriseResultLabel.text = currentWeather.sunrise
    weatherView.sunsetLabel.text = "Sunset".localized()
    weatherView.sunsetResultLabel.text = currentWeather.sunset
    weatherView.humidityLabel.text = "Humidity".localized()
    weatherView.humidityResultLabel.text = String(currentWeather.humidity ?? 0) + " %"
    weatherView.pressureLabel.text = "Pressure".localized()
        weatherView.pressureResultLabel.text = "\(String(currentWeather.pressure ?? 0)) " + "hPa".localized()
    weatherView.visibilityLabel.text = "Visibility".localized()
        let visibility = currentWeather.visibility ?? 0
        let visibilityInKm = visibility / 1000
        weatherView.visibilityResultLabel.text = "\(String(visibilityInKm)) " + "km".localized()
    weatherView.windLabel.text = "Wind".localized()
        let windDeg = currentWeather.wind_deg ?? 0
        let wind_speed = currentWeather.wind_speed ?? 0.0
    if windDeg > 0, windDeg < 90 {
        weatherView.windResultLabel.text = "\(String(describing: wind_speed)) " + "km/h".localized() + " " + "N".localized()
    } else if windDeg >= 90, windDeg < 180 {
        weatherView.windResultLabel.text = "\(String(describing: wind_speed)) " + "km/h".localized() + " " + "E".localized()
    } else if windDeg >= 180, windDeg < 270 {
        weatherView.windResultLabel.text = "\(String(describing: wind_speed)) " + "km/h".localized() + " " + "S".localized()
    } else {
        weatherView.windResultLabel.text = "\(String(describing: wind_speed)) " + "km/h".localized() + " " + "W".localized()
    }
    weatherView.rainLabel.text = "Rain".localized()
    weatherView.rainResultLabel.text = String(currentWeather.rain ?? 0.0)
    weatherView.cloudsLabel.text = "Clouds".localized()
    weatherView.cloudsResultLabel.text = String(currentWeather.clouds ?? 0) + " %"
    
        if cityLabel.textColor == .black {
            weatherView.cloudsLabel.textColor = .black
            weatherView.cloudsResultLabel.textColor = .black
            weatherView.humidityLabel.textColor = .black
            weatherView.humidityResultLabel.textColor = .black
            weatherView.pressureLabel.textColor = .black
            weatherView.pressureResultLabel.textColor = .black
            weatherView.rainLabel.textColor = .black
            weatherView.rainResultLabel.textColor = .black
            weatherView.sunriseLabel.textColor = .black
            weatherView.sunriseResultLabel.textColor = .black
            weatherView.sunsetLabel.textColor = .black
            weatherView.sunsetResultLabel.textColor = .black
            weatherView.visibilityLabel.textColor = .black
            weatherView.visibilityResultLabel.textColor = .black
            weatherView.windLabel.textColor = .black
            weatherView.windResultLabel.textColor = .black
        } else {
            weatherView.cloudsLabel.textColor = .white
            weatherView.cloudsResultLabel.textColor = .white
            weatherView.humidityLabel.textColor = .white
            weatherView.humidityResultLabel.textColor = .white
            weatherView.pressureLabel.textColor = .white
            weatherView.pressureResultLabel.textColor = .white
            weatherView.rainLabel.textColor = .white
            weatherView.rainResultLabel.textColor = .white
            weatherView.sunriseLabel.textColor = .white
            weatherView.sunriseResultLabel.textColor = .white
            weatherView.sunsetLabel.textColor = .white
            weatherView.sunsetResultLabel.textColor = .white
            weatherView.visibilityLabel.textColor = .white
            weatherView.visibilityResultLabel.textColor = .white
            weatherView.windLabel.textColor = .white
            weatherView.windResultLabel.textColor = .white
        }
    
    self.containerView.addSubview(self.weatherView)

    }
    
    func createDescriptionView() {
        descriptionView.frame = self.descrContainerView.bounds
        descriptionView.descriptionLabel.text = "Сегодня: сейчас преимущественно \(currentWeather.description ?? ""). Температура воздуха \(String(Int(currentWeather.temp?.rounded() ?? 0.0)))°. Ночью ожидается \(String(Int(currentWeather.nightTemp?.rounded() ?? 0.0)))°."
        descriptionView.descriptionLabel.numberOfLines = 0
        descriptionView.descriptionLabel.lineBreakMode = .byTruncatingTail
        descriptionView.descriptionLabel.minimumScaleFactor = 0.8
        if cityLabel.textColor == .white {
            descriptionView.descriptionLabel.textColor = .white
        }
        self.descrContainerView.addSubview(self.descriptionView)
    }
    
    func makingWhiteText() {
        cityLabel.textColor = .white
        currentTempLabel.textColor = .white
        maxTempLabel.textColor = .white
        minTempLabel.textColor = .white
        describtionLabel.textColor = .white
        dateLabel.textColor = .white
        feelsLikeTempLabel.textColor = .white
//        feelsLikeTextLabel.textColor = .white
        buttonAddedCity.tintColor = .white
        buttonDeletedCity.tintColor = .white
    }
    func makingBlackText() {
        cityLabel.textColor = .black
        currentTempLabel.textColor = .black
        maxTempLabel.textColor = .black
        minTempLabel.textColor = .black
        describtionLabel.textColor = .black
        dateLabel.textColor = .black
        feelsLikeTempLabel.textColor = .black
//        feelsLikeTextLabel.textColor = .black
        buttonAddedCity.tintColor = .black
        buttonDeletedCity.tintColor = .black

    }

}

// MARK: - Extension UICollectionView

extension WeatherViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (hourArray.count - 24)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourlyCollectionViewCell", for: indexPath) as? HourlyCollectionViewCell else {
            return UICollectionViewCell()
        }
        if cityLabel.textColor == .black {
            cell.hourLabel.textColor = .black
            cell.tempLabel.textColor = .black
        } else {
            cell.hourLabel.textColor = .white
            cell.tempLabel.textColor = .white
        }
            cell.setup(with: hourArray[indexPath.item].icon, text: hourArray[indexPath.item].time, temp: hourArray[indexPath.item].temp)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       return CGSize(width: 60, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

// MARK: - Extension UITableView

extension WeatherViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dayArray.count - 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DailyTableViewCell", for: indexPath) as? DailyTableViewCell else {
            return UITableViewCell()
        }
        if cityLabel.textColor == .black {
            cell.dayLabel.textColor = .black
            cell.minTempLabel.textColor = .black
            cell.maxTempLabel.textColor = .black
        } else {
            cell.dayLabel.textColor = .white
            cell.minTempLabel.textColor = .white
            cell.maxTempLabel.textColor = .white
        }
        cell.dayLabel.text = dayArray[indexPath.row + 1].day
        cell.iconImageView.image = UIImage(named: dayArray[indexPath.row + 1].icon ?? "")
        let nightTemp = Int(dayArray[indexPath.row + 1].nightTemp?.rounded() ?? 0.0)
        cell.minTempLabel.text = String(nightTemp) + " °"
        let maxTemp = Int(dayArray[indexPath.row + 1].dayTemp?.rounded() ?? 0.0)
        cell.maxTempLabel.text = String(maxTemp) + " °"
        cell.layer.backgroundColor = UIColor.clear.cgColor
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Extension ViewControllerDelegate

extension WeatherViewController: ViewControllerDelegate {
    
    func update(_ text: String) {
        print(text)
        
        self.array = WeatherArrayManager.shared.fullWeatherArray
        self.index = WeatherArrayManager.shared.fullWeatherArray.count
        self.delegate?.update(self.index ?? 0)
    }
    
}
