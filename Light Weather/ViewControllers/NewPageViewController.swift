import UIKit

class NewPageViewController: UIPageViewController {
    
    // MARK: - Public Properties

    var array = [ControllersModel]()
    var currentViewControllerIndex = 0
    var viewModel: WeatherControllerViewModel?
    var newWeatherArray = [ControllersModel]()
    var viewConrolls = [WeatherViewController]()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        
        self.array = WeatherArrayManager.shared.locationArray + WeatherArrayManager.shared.fullWeatherArray

        viewModel = WeatherControllerViewModel()
        viewModel?.update()
        
        if let weatherViewController = detailViewControllerAt(index: currentViewControllerIndex) {
            setViewControllers([weatherViewController], direction: .forward, animated: true, completion: nil)
        }
        
    }
    
    // MARK: - Flow functions

    func detailViewControllerAt(index: Int) -> WeatherViewController? {
        
        guard index >= 0 else {return nil}
        guard index <= array.count else {return nil}
        
        guard let weatherController = self.storyboard?.instantiateViewController(withIdentifier: "WeatherViewController") as? WeatherViewController else {
            return nil
        }
        
        weatherController.delegate = self
        weatherController.index = index
        weatherController.hourArray = array[index].hourlyWeather ?? []
        weatherController.dayArray = array[index].dailyWeather ?? []
        weatherController.currentWeather = array[index].currentWeather ?? WeatherInfo(date: "", city: "", main: "", description: "", temp: 0.0, maxTemp: 0.0, nightTemp: 0.0, visibility: 0, pressure: 0, humidity: 0, sunrise: "", sunset: "", clouds: 0, rain: 0.0, icon: "", wind_speed: 0.0, wind_deg: 0, feels_like: 0.0)
        
        return weatherController
    }
    
}

// MARK: - Extension UIPageViewController

extension NewPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard var currentIndex = (viewController as? WeatherViewController)?.index else {return nil}
        
        currentViewControllerIndex = currentIndex
        
        if currentIndex == 0 {
            return nil
        }
        currentIndex -= 1
        
        return detailViewControllerAt(index: currentIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard var currentIndex = (viewController as? WeatherViewController)?.index else {return nil}
        
        currentViewControllerIndex = currentIndex
 
        if currentIndex == array.count - 1 {
            return nil
        }
        currentIndex += 1
        
        return detailViewControllerAt(index: currentIndex)
    }
}

// MARK: - Extension WeatherViewControllerDelegate

extension NewPageViewController: WeatherViewControllerDelegate {
    func deleteCity(_ index: Int) {
        if index == 0 {
            print("It's impossible to delete")
        } else {
        var changedArray = WeatherArrayManager.shared.fullWeatherArray
        changedArray.remove(at: index - 1)
        var coordArray = SaveWeatherManager.shared.getCoordinatesArray()
        coordArray?.remove(at: index - 1)
        WeatherArrayManager.shared.fullWeatherArray = changedArray
        SaveWeatherManager.shared.setCoordinatesArray(coordArray ?? [])
        array = WeatherArrayManager.shared.locationArray + WeatherArrayManager.shared.fullWeatherArray
        if let weatherViewController = detailViewControllerAt(index: index - 1) {
            setViewControllers([weatherViewController], direction: .forward, animated: true, completion: nil)
        }
        }
    }
    
    func update(_ index: Int) {
        array = WeatherArrayManager.shared.locationArray + WeatherArrayManager.shared.fullWeatherArray

        if let weatherViewController = detailViewControllerAt(index: array.count - 1) {
            setViewControllers([weatherViewController], direction: .forward, animated: true, completion: nil)
        }
        self.currentViewControllerIndex = array.count - 1

    }
}
