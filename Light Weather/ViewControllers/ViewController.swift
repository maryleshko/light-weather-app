import UIKit
import CoreLocation

// MARK: - Protocols

protocol ViewControllerDelegate: AnyObject {
    func update(_ text: String)
}

struct CityDescription: Decodable {
    var lon: Double?
    var lat: Double?
    var timezone: String?
    var current: Current?
    var hourly: [Hourly]?
    var daily: [Daily]?
}
struct Current: Decodable {
    var dt: Int
    var sunrise: Int?
    var sunset: Int?
    var temp: Double?
    var feels_like: Double?
    var pressure: Int?
    var humidity: Int?
    var dew_point: Double?
    var uvi: Double?
    var clouds: Int?
    var visibility: Int?
    var wind_speed: Double?
    var wind_deg: Int?
    var weather: [CurrentWeather]?
    var rain: Rain?
}
struct CurrentWeather: Decodable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}
struct Rain: Decodable {
    var oneH: Double?
}
struct Minutely: Decodable {
    var dt: Date?
    var precipitation: Double?
}
struct Hourly: Decodable {
    var dt: Int
    var temp: Double?
    var feels_like: Double?
    var pressure: Int?
    var humidity: Int?
    var dew_point: Double?
    var clouds: Int?
    var wind_speed: Double?
    var wind_deg: Int?
    var weather: [CurrentWeather]?
    var rain: Rain?
}
struct Daily: Decodable {
    var dt: Int
    var sunrise: Int?
    var sunset: Int?
    var temp: Temp?
    var feels_like: FeelsLike?
    var pressure: Int?
    var humidity: Int?
    var dew_point: Double?
    var wind_speed: Double?
    var wind_deg: Int?
    var weather: [CurrentWeather]?
    var clouds: Int?
    var rain: Double?
    var uvi: Double?
}
struct Temp: Decodable {
    var day: Double?
    var min: Double?
    var max: Double?
    var night: Double?
    var eve: Double?
    var morn: Double?
}
struct FeelsLike: Decodable {
    var day: Double?
    var night: Double?
    var eve: Double?
    var morn: Double?
}

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    // MARK: - IBOutlets

    @IBOutlet weak var buttonGoBack: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Public Properties

    var newArray = [City]()
    var searchResults = [City]()
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    var name = String()
    weak var delegate: ViewControllerDelegate?
    var viewModel: TableViewViewModel?
    let searchController = UISearchController(searchResultsController: nil)

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = TableViewViewModel()
        viewModel?.update()
        bindOutlets()
        
        tableView.delegate = self
        tableView.tableHeaderView = searchController.searchBar
        tableView.tableHeaderView?.backgroundColor = .white
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    // MARK: - IBActions

    @IBAction func buttonGoBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Private functions

    private func bindOutlets() {
        self.viewModel?.textBackToMenu.bind({ (value) in
            self.buttonGoBack.setTitle(value, for: .normal)
        })
        self.viewModel?.searchBarPlaceholder.bind({ (value) in
            self.searchController.searchBar.placeholder = value
        })
    }
}

// MARK: - Extension UITableView

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return searchResults.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        
        let city: City
        if isFiltering {
            city = searchResults[indexPath.row]
            
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.customLabel.text = city.name
            cell.countryLabel.text = city.country
            cell.tag = indexPath.row
            
        } else {
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.customLabel.text = ""
            cell.countryLabel.text = ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city: City
        if isFiltering {
            city = searchResults[indexPath.row]
        } else {
            city = newArray[indexPath.row]
        }
        let lat = city.coord?.lat ?? 0.0
        let lon = city.coord?.lon ?? 0.0
        let name = city.name
        JSONManager.shared.openWeatherJSON(lat: lat, lon: lon, name: name) { (_) in
            DispatchQueue.main.async {
                
                self.delegate?.update("HI")
                self.navigationController?.popViewController(animated: true)

            }
            
        }
    }
    
}

// MARK: - Extension UISearchResultsUpdating

extension ViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
    private func filterContentForSearchText(_ searchText: String) {
        searchResults = newArray.filter { (city: City) -> Bool in
            return city.name.lowercased().contains(searchText.lowercased())
        }
        
        tableView.reloadData()
    }
}
