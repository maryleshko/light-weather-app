import Foundation

class CityCoordinates: Codable {
    var coordinates: Coordinates?
    var name: String?
    
    init() {}
    
    init(coordinates: Coordinates, name: String) {
        self.coordinates = coordinates
        self.name = name
    }
    
    public enum CodingKeys: String, CodingKey {
        case coordinates, name
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.coordinates = try container.decodeIfPresent(Coordinates.self, forKey: .coordinates)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.coordinates, forKey: .coordinates)
        try container.encode(self.name, forKey: .name)
        
    }
    
}
