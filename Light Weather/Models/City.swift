import Foundation

class City {
    var id: Int = 0
    var name: String = ""
    var state: String = ""
    var country: String = ""
    var coord : Coordinates?
}
