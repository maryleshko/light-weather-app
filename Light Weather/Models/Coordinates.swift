import Foundation

class Coordinates: Codable {
    var lon: Double?
    var lat: Double?
    
    init() {}
    
    init(lon: Double, lat: Double) {
        self.lon = lon
        self.lat = lat
    }
    
    public enum CodingKeys: String, CodingKey {
        case lon, lat
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.lon = try container.decodeIfPresent(Double.self, forKey: .lon)
        self.lat = try container.decodeIfPresent(Double.self, forKey: .lat)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.lon, forKey: .lon)
        try container.encode(self.lat, forKey: .lat)
        
    }
    
}
