import UIKit

class ControllersModel: Codable {

    
    var currentWeather: WeatherInfo?
    var hourlyWeather: [HourlyWeather]?
    var dailyWeather:[DailyWeather]?
    
    
    init() {}
    
    init(currentWeather: WeatherInfo?, hourlyWeather: [HourlyWeather]?, dailyWeather: [DailyWeather]?) {
        self.currentWeather = currentWeather
        self.hourlyWeather = hourlyWeather
        self.dailyWeather = dailyWeather
    }
    
    public enum CodingKeys: String, CodingKey {
        case currentWeather, hourlyWeather, dailyWeather
    }
   
    required public init(from decoder: Decoder) throws {
           let container = try decoder.container(keyedBy: CodingKeys.self)
        self.currentWeather = try container.decodeIfPresent(WeatherInfo.self, forKey: .currentWeather)
        self.hourlyWeather = try container.decodeIfPresent([HourlyWeather].self, forKey: .hourlyWeather)
        self.dailyWeather = try container.decodeIfPresent([DailyWeather].self, forKey: .dailyWeather)
       }

       public func encode(to encoder: Encoder) throws {
           var container = encoder.container(keyedBy: CodingKeys.self)
           try container.encode(self.currentWeather, forKey: .currentWeather)
        try container.encode(self.hourlyWeather, forKey: .hourlyWeather)
        try container.encode(self.dailyWeather, forKey: .dailyWeather)
       }

}

