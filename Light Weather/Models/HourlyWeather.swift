import Foundation

class HourlyWeather: Codable {
    var time: String?
    var temp: Double?
    var icon: String?
   
    init() {}

    init(time: String?, temp: Double?, icon: String?) {
        self.time = time
        self.temp = temp
        self.icon = icon
    }
    
    public enum CodingKeys: String, CodingKey {
         case time, temp, icon
     }
    
     required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
         self.time = try container.decodeIfPresent(String.self, forKey: .time)
         self.temp = try container.decodeIfPresent(Double.self, forKey: .temp)
         self.icon = try container.decodeIfPresent(String.self, forKey: .icon)
        }

        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(self.time, forKey: .time)
         try container.encode(self.temp, forKey: .temp)
         try container.encode(self.icon, forKey: .icon)
        }
}
