import Foundation

class WeatherInfo: Codable {
    var date: String?
    var city: String?
    var main: String?
    var description: String?
    var temp: Double?
    var maxTemp: Double?
    var nightTemp: Double?
    var visibility: Int?
    var pressure: Int?
    var humidity: Int?
    var sunrise: String?
    var sunset: String?
    var clouds: Int?
    var rain: Double?
    var icon: String?
    var wind_speed: Double?
    var wind_deg: Int?
    var feels_like: Double?
    
    init() {}
    
    init(date: String, city: String, main: String, description: String, temp: Double, maxTemp: Double, nightTemp: Double, visibility: Int, pressure: Int, humidity: Int, sunrise: String, sunset: String, clouds: Int, rain: Double, icon: String, wind_speed: Double, wind_deg: Int, feels_like: Double) {
        self.date = date
        self.city = city
        self.main = main
        self.description = description
        self.temp = temp
        self.maxTemp = maxTemp
        self.nightTemp = nightTemp
        self.visibility = visibility
        self.pressure = pressure
        self.humidity = humidity
        self.sunrise = sunrise
        self.sunset = sunset
        self.clouds = clouds
        self.rain = rain
        self.icon = icon
        self.wind_speed = wind_speed
        self.wind_deg = wind_deg
        self.feels_like = feels_like
    }
    public enum CodingKeys: String, CodingKey {
        case date, city, main, description, temp, maxTemp, nightTemp, visibility, pressure, humidity, sunrise, sunset, clouds, rain, icon, wind_speed, wind_deg, feels_like
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.date = try container.decodeIfPresent(String.self, forKey: .date)
        self.city = try container.decodeIfPresent(String.self, forKey: .city)
        self.main = try container.decodeIfPresent(String.self, forKey: .main)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        self.temp = try container.decodeIfPresent(Double.self, forKey: .temp)
        self.maxTemp = try container.decodeIfPresent(Double.self, forKey: .maxTemp)
        self.nightTemp = try container.decodeIfPresent(Double.self, forKey: .nightTemp)
        self.visibility = try container.decodeIfPresent(Int.self, forKey: .visibility)
        self.pressure = try container.decodeIfPresent(Int.self, forKey: .pressure)
        self.humidity = try container.decodeIfPresent(Int.self, forKey: .humidity)
        self.sunrise = try container.decodeIfPresent(String.self, forKey: .sunrise)
        self.sunset = try container.decodeIfPresent(String.self, forKey: .sunset)
        self.clouds = try container.decodeIfPresent(Int.self, forKey: .clouds)
        self.rain = try container.decodeIfPresent(Double.self, forKey: .rain)
        self.icon = try container.decodeIfPresent(String.self, forKey: .icon)
        self.wind_speed = try container.decodeIfPresent(Double.self, forKey: .wind_speed)
        self.wind_deg = try container.decodeIfPresent(Int.self, forKey: .wind_deg)
        self.feels_like = try container.decodeIfPresent(Double.self, forKey: .feels_like)
        
        
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.date, forKey: .date)
        try container.encode(self.city, forKey: .city)
        try container.encode(self.main, forKey: .main)
        try container.encode(self.description, forKey: .description)
        try container.encode(self.temp, forKey: .temp)
        try container.encode(self.maxTemp, forKey: .maxTemp)
        try container.encode(self.nightTemp, forKey: .nightTemp)
        try container.encode(self.visibility, forKey: .visibility)
        try container.encode(self.pressure, forKey: .pressure)
        try container.encode(self.humidity, forKey: .humidity)
        try container.encode(self.sunrise, forKey: .sunrise)
        try container.encode(self.sunset, forKey: .sunset)
        try container.encode(self.clouds, forKey: .clouds)
        try container.encode(self.rain, forKey: .rain)
        try container.encode(self.icon, forKey: .icon)
        try container.encode(self.wind_speed, forKey: .wind_speed)
        try container.encode(self.wind_deg, forKey: .wind_deg)
        try container.encode(self.feels_like, forKey: .feels_like)
        
        
    }
}
