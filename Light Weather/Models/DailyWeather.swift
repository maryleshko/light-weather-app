import Foundation

class DailyWeather: Codable {
    var day: String?
    var dayTemp: Double?
    var nightTemp: Double?
    var icon: String?
    
    init() {}

    init(day: String, dayTemp: Double?, nightTemp: Double?, icon: String?) {
        self.day = day
        self.dayTemp = dayTemp
        self.nightTemp = nightTemp
        self.icon = icon
    }
    
    public enum CodingKeys: String, CodingKey {
         case day, dayTemp, nightTemp, icon
     }
    
     required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
         self.day = try container.decodeIfPresent(String.self, forKey: .day)
         self.dayTemp = try container.decodeIfPresent(Double.self, forKey: .dayTemp)
        self.nightTemp = try container.decodeIfPresent(Double.self, forKey: .nightTemp)
         self.icon = try container.decodeIfPresent(String.self, forKey: .icon)
        }

        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(self.day, forKey: .day)
         try container.encode(self.dayTemp, forKey: .dayTemp)
            try container.encode(self.nightTemp, forKey: .nightTemp)

         try container.encode(self.icon, forKey: .icon)
        }
    
    func getDay() -> String {
        return self.day ?? ""
    }
    
    func getIcon() -> String {
        return self.icon ?? ""
    }
}
