import Foundation
import UIKit

class SaveWeatherManager {
    static let shared = SaveWeatherManager()
    
    private var defaultWeather = ControllersModel(currentWeather: WeatherInfo(date: "", city: "", main: "", description: "", temp: 0.0, maxTemp: 0.0, nightTemp: 0.0, visibility: 0, pressure: 0, humidity: 0, sunrise: "", sunset: "", clouds: 0, rain: 0.0, icon: "", wind_speed: 0.0, wind_deg: 0, feels_like: 0.0), hourlyWeather:[], dailyWeather: [])
    let key = "weather"
    let coord = "coord"

    func getArray() -> [ControllersModel]? {
        if let weatherArray = UserDefaults.standard.value([ControllersModel].self, forKey: key) {
            return weatherArray
        }
        return []
    }
    func setWeather(_ weather: ControllersModel) {
        var weatherArray = self.getArray()
        weatherArray?.append(weather)
        self.setArray(weatherArray ?? [])
        print(weatherArray?.count)
    }
    func setArray(_ weatherArray: [ControllersModel]) {
        UserDefaults.standard.set(encodable: weatherArray, forKey: key)
    }
    
    func getCoordinatesArray() -> [CityCoordinates]? {
        if let coordinatesArray = UserDefaults.standard.value([CityCoordinates].self, forKey: coord) {
            return coordinatesArray
        }
        return []
    }
    func setCoordinates(_ coordinates: CityCoordinates) {
        var coordinatesArray = self.getCoordinatesArray()
        coordinatesArray?.append(coordinates)
        self.setCoordinatesArray(coordinatesArray ?? [])
        print(coordinatesArray?.count)
    }
    func setCoordinatesArray(_ coordinatesArray: [CityCoordinates]) {
        UserDefaults.standard.set(encodable: coordinatesArray, forKey: coord)
    }
    
    
    
    
    
}




extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
