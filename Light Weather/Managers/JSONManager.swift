import Foundation
import UIKit

class JSONManager {
    static var shared: JSONManager = JSONManager()
    var verification = false
    private init(){}
    
    func openJSON(completion: @escaping (_ cities: [City]) -> ()) {
        if let path = Bundle.main.path(forResource: "city.list", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                print()
                var array = [City]()
                var sortedArray = [City]()
                if let json = jsonResult as? [Any]{
                    
                    for element in json {
                        if let element = element as? [String: Any] {
                            let city = City()
                            if let id = element["id"] as? Int {
                                city.id = id
                            }
                            if let name = element["name"] as? String {
                                city.name = name
                            }
                            if let state = element["state"] as? String {
                                city.state = state
                            }
                            if let country = element["country"] as? String {
                                city.country = country
                            }
                            if let coord = element["coord"] as? [String: Any] {
                                let coordinates = Coordinates()
                                if let lon = coord["lon"] as? Double {
                                    coordinates.lon = lon
                                }
                                if let lat = coord["lat"] as? Double {
                                    coordinates.lat = lat
                                }
                                city.coord = coordinates
                            }
                            array.append(city)
                            
                        }
                    }
                    sortedArray = array.sorted (by: {$0.name < $1.name})
                    completion(sortedArray)
                    WeatherArrayManager.shared.cityArray = sortedArray
                }
            } catch {
                // handle error
            }
        }
    }
    
    func openWeatherJSON(lat: Double, lon: Double, name: String, completion: @escaping (_ weather: ControllersModel) -> ()) {
        let baseURL = "https://api.openweathermap.org/data/2.5/onecall?lat="
        let urlString = baseURL + "\(lat)&lon=\(lon)&exclude=minutely&appid=cd339fd2cc0f489fd08b49ae5bce3e45&units=metric&lang=ru"
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let coordin = Coordinates()
        coordin.lat = lat
        coordin.lon = lon
        
        let cityCoordinates = CityCoordinates()
        cityCoordinates.coordinates = coordin
        cityCoordinates.name = name
        
        
        
        guard let coordArray = SaveWeatherManager.shared.getCoordinatesArray() else {return}
        
//        SaveWeatherManager.shared.setCoordinates(cityCoordinates)
        for element in coordArray {

            if element.coordinates?.lat == cityCoordinates.coordinates?.lat, element.coordinates?.lon == cityCoordinates.coordinates?.lon {
                verification = true
            }
           
        }
          if verification == false {
            SaveWeatherManager.shared.setCoordinates(cityCoordinates)
            verification = false
        }
        verification = false
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                let data = data else { return }
            // do-try-catch
            do {
                let object = try JSONDecoder().decode(CityDescription.self, from: data)
                //                print(object)
                let fullWeather = ControllersModel()
                var hourlyWeather = [HourlyWeather]()
                var dailyWeather = [DailyWeather]()
                
                let calendar = Calendar.current
                let date = JSONManager.shared.unixToDate(object.current?.dt ?? 0)
                let currentDate = JSONManager.shared.dateToDate(date ?? NSDate.distantFuture)
                let sunr = JSONManager.shared.unixToDate(object.current?.sunrise ?? 0)
                let sunrise = JSONManager.shared.dateToSun(sunr)
                let suns = JSONManager.shared.unixToDate(object.current?.sunset ?? 0)
                let sunset = JSONManager.shared.dateToSun(suns)
                
                
                
                
                let weather = WeatherInfo(date: currentDate, city: name, main: object.current?.weather?[0].main ?? "", description: object.current?.weather?[0].description ?? "", temp: object.current?.temp ?? 0.0, maxTemp: object.daily?[0].temp?.max ?? 0.0, nightTemp: object.daily?[0].temp?.night ?? 0.0, visibility: object.current?.visibility ?? 0, pressure: object.current?.pressure ?? 0, humidity: object.current?.humidity ?? 0, sunrise: sunrise, sunset: sunset, clouds: object.current?.clouds ?? 0, rain: object.daily?[0].rain ?? 0.0, icon: object.current?.weather?[0].icon ?? "", wind_speed: object.current?.wind_speed ?? 0.0, wind_deg: object.current?.wind_deg ?? 0, feels_like: object.current?.feels_like ?? 0.0)
                fullWeather.currentWeather = weather
                WeatherArrayManager.shared.weatherArray.append(weather)
                for element in object.hourly ?? [] {
                    let date = JSONManager.shared.unixToDate(element.dt)
                    //                    var newHourly = [HourlyWeather]()
                    let hour = JSONManager.shared.dateToHour(date)
                    let hourWeather = HourlyWeather(time: hour, temp: element.temp, icon: element.weather?[0].icon)
                    hourlyWeather.append(hourWeather)
                    
                }
                for element in object.daily ?? [] {
                    let date = JSONManager.shared.unixToDate(element.dt)
                    //                    var newDaily = [DailyWeather]()
                    let day = JSONManager.shared.dateToDay(date)
                    let dayWeather = DailyWeather(day: day, dayTemp: element.temp?.max, nightTemp: element.temp?.night, icon: element.weather?[0].icon)
                    dailyWeather.append(dayWeather)
                    
                }
                //                completion(dailyWeather)
                print()
                
                
                fullWeather.hourlyWeather = hourlyWeather
                fullWeather.dailyWeather = dailyWeather
                
                WeatherArrayManager.shared.fullWeatherArray.append(fullWeather)
                
                //                SaveWeatherManager.shared.setWeather(fullWeather)
                completion(fullWeather)
                
                //if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                //                                    print() }
                
            } catch let error {
                print(error)
            }
        }
        task.resume()
    }
    
    func secondOpenWeatherJSON(lat: Double, lon: Double, name: String, completion: @escaping (_ weather: ControllersModel) -> ()) {
            let baseURL = "https://api.openweathermap.org/data/2.5/onecall?lat="
            let urlString = baseURL + "\(lat)&lon=\(lon)&exclude=minutely&appid=cd339fd2cc0f489fd08b49ae5bce3e45&units=metric&lang=ru"
            let url = URL(string: urlString)!
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard error == nil,
                    let data = data else { return }
                // do-try-catch
                do {
                    let object = try JSONDecoder().decode(CityDescription.self, from: data)
                    //                print(object)
                    var fullWeather = ControllersModel()
                    var hourlyWeather = [HourlyWeather]()
                    var dailyWeather = [DailyWeather]()
                    
                    let calendar = Calendar.current
                    let date = JSONManager.shared.unixToDate(object.current?.dt ?? 0)
                    let currentDate = JSONManager.shared.dateToDate(date ?? NSDate.distantFuture)
                    let sunr = JSONManager.shared.unixToDate(object.current?.sunrise ?? 0)
                    let sunrise = JSONManager.shared.dateToSun(sunr)
                    let suns = JSONManager.shared.unixToDate(object.current?.sunset ?? 0)
                    let sunset = JSONManager.shared.dateToSun(suns)
                    
                    
                    
                    
                    let weather = WeatherInfo(date: currentDate, city: name, main: object.current?.weather?[0].main ?? "", description: object.current?.weather?[0].description ?? "", temp: object.current?.temp ?? 0.0, maxTemp: object.daily?[0].temp?.max ?? 0.0, nightTemp: object.daily?[0].temp?.night ?? 0.0, visibility: object.current?.visibility ?? 0, pressure: object.current?.pressure ?? 0, humidity: object.current?.humidity ?? 0, sunrise: sunrise, sunset: sunset, clouds: object.current?.clouds ?? 0, rain: object.current?.rain?.oneH ?? 0.0, icon: object.current?.weather?[0].icon ?? "", wind_speed: object.current?.wind_speed ?? 0.0, wind_deg: object.current?.wind_deg ?? 0, feels_like: object.current?.feels_like ?? 0.0)
                    fullWeather.currentWeather = weather
                    WeatherArrayManager.shared.weatherArray.append(weather)
                    for element in object.hourly ?? [] {
                        let date = JSONManager.shared.unixToDate(element.dt)
                        //                    var newHourly = [HourlyWeather]()
                        let hour = JSONManager.shared.dateToHour(date)
                        let hourWeather = HourlyWeather(time: hour, temp: element.temp, icon: element.weather?[0].icon)
                        hourlyWeather.append(hourWeather)
                        
                    }
                    for element in object.daily ?? [] {
                        let date = JSONManager.shared.unixToDate(element.dt)
                        //                    var newDaily = [DailyWeather]()
                        let day = JSONManager.shared.dateToDay(date)
                        let dayWeather = DailyWeather(day: day, dayTemp: element.temp?.max, nightTemp: element.temp?.night, icon: element.weather?[0].icon)
                        dailyWeather.append(dayWeather)
                        
                    }
                    //                completion(dailyWeather)
                    print()
                    
                    
                    fullWeather.hourlyWeather = hourlyWeather
                    fullWeather.dailyWeather = dailyWeather
                    
                    WeatherArrayManager.shared.fullWeatherArray.append(fullWeather)
                    
                    //                SaveWeatherManager.shared.setWeather(fullWeather)
                    completion(fullWeather)
                    
                    //if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                    //                                    print() }
                    
                } catch let error {
                    print(error)
                }
            }
            task.resume()
        }
    func locationOpenWeatherJSON(lat: Double, lon: Double, name: String, completion: @escaping (_ weather: ControllersModel) -> ()) {
            let baseURL = "https://api.openweathermap.org/data/2.5/onecall?lat="
            let urlString = baseURL + "\(lat)&lon=\(lon)&exclude=minutely&appid=cd339fd2cc0f489fd08b49ae5bce3e45&units=metric&lang=ru"
            let url = URL(string: urlString)!
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard error == nil,
                    let data = data else { return }
                // do-try-catch
                do {
                    let object = try JSONDecoder().decode(CityDescription.self, from: data)
                    //                print(object)
                    var fullWeather = ControllersModel()
                    var hourlyWeather = [HourlyWeather]()
                    var dailyWeather = [DailyWeather]()
                    
                    let calendar = Calendar.current
                    let date = JSONManager.shared.unixToDate(object.current?.dt ?? 0)
                    let currentDate = JSONManager.shared.dateToDate(date ?? NSDate.distantFuture)
                    let sunr = JSONManager.shared.unixToDate(object.current?.sunrise ?? 0)
                    let sunrise = JSONManager.shared.dateToSun(sunr)
                    let suns = JSONManager.shared.unixToDate(object.current?.sunset ?? 0)
                    let sunset = JSONManager.shared.dateToSun(suns)
                    
                    
                    
                    
                    let weather = WeatherInfo(date: currentDate, city: name, main: object.current?.weather?[0].main ?? "", description: object.current?.weather?[0].description ?? "", temp: object.current?.temp ?? 0.0, maxTemp: object.daily?[0].temp?.max ?? 0.0, nightTemp: object.daily?[0].temp?.night ?? 0.0, visibility: object.current?.visibility ?? 0, pressure: object.current?.pressure ?? 0, humidity: object.current?.humidity ?? 0, sunrise: sunrise, sunset: sunset, clouds: object.current?.clouds ?? 0, rain: object.current?.rain?.oneH ?? 0.0, icon: object.current?.weather?[0].icon ?? "", wind_speed: object.current?.wind_speed ?? 0.0, wind_deg: object.current?.wind_deg ?? 0, feels_like: object.current?.feels_like ?? 0.0)
                    fullWeather.currentWeather = weather
                    WeatherArrayManager.shared.weatherArray.append(weather)
                    for element in object.hourly ?? [] {
                        let date = JSONManager.shared.unixToDate(element.dt)
                        let hour = JSONManager.shared.dateToHour(date)
                        let hourWeather = HourlyWeather(time: hour, temp: element.temp, icon: element.weather?[0].icon)
                        hourlyWeather.append(hourWeather)
                        
                    }
                    for element in object.daily ?? [] {
                        let date = JSONManager.shared.unixToDate(element.dt)
                        //                    var newDaily = [DailyWeather]()
                        let day = JSONManager.shared.dateToDay(date)
                        let dayWeather = DailyWeather(day: day, dayTemp: element.temp?.max, nightTemp: element.temp?.night, icon: element.weather?[0].icon)
                        dailyWeather.append(dayWeather)
                        
                    }
                    print()
                    
                    
                    fullWeather.hourlyWeather = hourlyWeather
                    fullWeather.dailyWeather = dailyWeather
                    WeatherArrayManager.shared.locationArray.append(fullWeather)
                    completion(fullWeather)
                                        
                } catch let error {
                    print(error)
                }
            }
            task.resume()
        }
    func dateToDay(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        formatter.locale = NSLocale.current
        formatter.calendar = Calendar.current
        formatter.dateFormat = "EEEE"
        
        return formatter.string(from: date)
    }
    func dateToHour(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.calendar = Calendar.current
        formatter.dateFormat = "HH"
        
        return formatter.string(from: date)
    }
    func dateToDate(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.calendar = Calendar.current
        formatter.dateFormat = "E, d MMM"
        
        return formatter.string(from: date)
    }
    func dateToSun(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.calendar = Calendar.current
        formatter.dateFormat = "HH:mm"
        
        return formatter.string(from: date)
    }
    func unixToDate(_ unix: Int) -> Date {
        let date = Date(timeIntervalSince1970: TimeInterval(unix))
        return date
    }
    
}
