import UIKit

class DescriptionView: UIView {
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    static func instanceFromNib() -> DescriptionView {
        return UINib(nibName: "DescriptionView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? DescriptionView ?? DescriptionView()
        
    }
}
